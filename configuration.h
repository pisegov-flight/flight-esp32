#include <Arduino.h>
#include "secrets.h"

// constants are defined in secrets.h like this
// #define WIFI_SSID "ssid"
// #define WIFI_PASSWORD "password"
// #define OUTPUT_PIN 10

extern const char* ssid = WIFI_SSID;
extern const char* password = WIFI_PASSWORD;
extern const int output = OUTPUT_PIN;

// WireGuard configuration
extern const char private_key[] = WG_PRIVATE_KEY;
extern const IPAddress local_ip(10,0,0,27);
extern const char public_key[] = WG_PUBLIC_KEY;
extern const char endpoint_address[] = WG_ENDPOINT_ADDRESS;
extern const int endpoint_port = WG_ENDPOINT_PORT;
