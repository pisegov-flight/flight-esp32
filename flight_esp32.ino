// Загружаем библиотеку Wi-Fi
#include <WiFi.h>
#include <WireGuard-ESP32.h>
#include <HTTPClient.h>
#include "page.cpp"
#include "configuration.h"
WiFiServer server(80);

String header;

String outputState = "off";

static WireGuard wg;
static HTTPClient httpClient;

static constexpr const uint32_t UPDATE_INTERVAL_MS = 5000;

void setup() {
  Serial.begin(115200);
  pinMode(output, OUTPUT);
  digitalWrite(output, LOW);

  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected.");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  Serial.println("Adjusting system time...");
  configTime(3 * 60 * 60, 0, "time.google.com");

  Serial.println("Connected. Initializing WireGuard...");
  wg.begin(
    local_ip,
    private_key,
    endpoint_address,
    public_key,
    endpoint_port);

  Serial.println("Done");

  server.begin();

}

void loop() {
  WiFiClient client = server.available();

  if (client) {
    Serial.println("New Client.");
    String currentLine = "";
    while (client.connected()) {
      if (client.available()) {
        char c = client.read();
        Serial.write(c);
        header += c;
        if (c == '\n') {
          if (currentLine.length() == 0) {
            client.println("HTTP/1.1 200 OK");
            client.println("Content-type:text/html");
            client.println("Connection: close");
            client.println();

            if (header.indexOf("GET /26/on") >= 0) {
              Serial.println("GPIO 26 on");
              outputState = "on";
              digitalWrite(output, HIGH);
            } else if (header.indexOf("GET /26/off") >= 0) {
              Serial.println("GPIO 26 off");
              outputState = "off";
              digitalWrite(output, LOW);
            }

            break;
          } else {
            currentLine = "";
          }
        } else if (c != '\r') {
          currentLine += c;
        }
      }
    }

    header = "";
    client.stop();
    Serial.println("Client disconnected.");
    Serial.println("");
  }
}
